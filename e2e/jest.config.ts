/* eslint-disable */
export default {
  displayName: 'e2e',
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: 'junit/e2e',
        suiteName: 'workspace',
        uniqueOutputName: true,
        classNameTemplate: '{classname}',
        titleTemplate: '{title}',
        usePathForSuiteName: 'true'
      }
    ]
  ],
  preset: '../jest.preset.js',
  globalSetup: '<rootDir>/src/support/global-setup.ts',
  globalTeardown: '<rootDir>/src/support/global-teardown.ts',
  setupFiles: [ '<rootDir>/src/support/test-setup.ts' ],
  testEnvironment: 'node',
  transform: {
    '^.+\\.[tj]s$': [
      'ts-jest', {
        tsconfig: '<rootDir>/tsconfig.spec.json'
      }
    ]
  },
  moduleFileExtensions: [ 'ts', 'js', 'html' ],
  coverageDirectory: '../coverage/e2e'
};
