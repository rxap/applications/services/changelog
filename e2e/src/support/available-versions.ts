export const AVAILABLE_VERSIONS = [
  '1.3.4',
  '1.3.5',
  '2.4.5',
  '2.4.6-rc.0'
];
