import axios from 'axios';
import { AVAILABLE_VERSIONS } from '../support/available-versions';

describe('GET /api/changelog/:version/:application', () => {
  it.each(AVAILABLE_VERSIONS)('should return the changelog for version %s for the app `my-app`', async (version) => {
    const res = await axios.get(`/api/changelog/${ version }/my-app`);

    expect(res.status).toBe(200);
    expect(res.data).toEqual({
      'application': [
        `# My App ${ version } Changelog\n`
      ],
      'general': [
        `# General ${ version } Changelog\n`
      ]
    });

  });

  it.each(AVAILABLE_VERSIONS)('should return the changelog for version %s for a undefined app', async (version) => {
    const res = await axios.get(`/api/changelog/${ version }/undefined-app`);

    expect(res.status).toBe(200);
    expect(res.data).toEqual({
      'application': [],
      'general': [
        `# General ${ version } Changelog\n`
      ]
    });

  });

});
