import axios from 'axios';

describe('GET /api/changelog', () => {
  it('should return a message', async () => {
    const res = await axios.get(`/api/changelog`);

    expect(res.status).toBe(200);
    expect(res.data).toEqual('changelog');

  });
});
