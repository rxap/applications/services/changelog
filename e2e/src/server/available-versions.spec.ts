import axios from 'axios';
import { AVAILABLE_VERSIONS } from '../support/available-versions';

describe('GET /api/changelog/available-versions', () => {
  it('should return the list of available versions', async () => {
    const res = await axios.get(`/api/changelog/available-versions`);

    expect(res.status).toBe(200);
    expect(res.data).toEqual([
      ...AVAILABLE_VERSIONS,
      'latest'
    ]);

  });
});
