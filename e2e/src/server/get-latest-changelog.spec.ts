import axios from 'axios';

describe('GET /api/changelog/latest/:application', () => {
  it('should return the latest change for the app `my-app`', async () => {
    const res = await axios.get(`/api/changelog/latest/my-app`);

    expect(res.status).toBe(200);
    expect(res.data).toEqual({
      'application': [
        '# My App Latest Changelog\n'
      ],
      'general': [
        '# General Latest Changelog\n'
      ]
    });

  });

  it('should return the latest change for a undefined app', async () => {
    const res = await axios.get(`/api/changelog/latest/undefined-app`);

    expect(res.status).toBe(200);
    expect(res.data).toEqual({
      'application': [],
      'general': [
        '# General Latest Changelog\n'
      ]
    });

  });

});
