/* eslint-disable */
export default {
  displayName: 'workspace',
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: 'junit/workspace',
        suiteName: 'workspace',
        uniqueOutputName: true,
        classNameTemplate: '{classname}',
        titleTemplate: '{title}',
        usePathForSuiteName: 'true'
      }
    ]
  ],
  preset: './jest.preset.js',
  testEnvironment: 'node',
  transform: {
    '^.+\\.[tj]s$': [ 'ts-jest', { tsconfig: '<rootDir>/tsconfig.spec.json' } ]
  },
  moduleFileExtensions: [ 'ts', 'js', 'html' ],
  coverageDirectory: './coverage/workspace',
  testMatch: [
    '<rootDir>/src/**/__tests__/**/*.[jt]s?(x)',
    '<rootDir>/src/**/*(*.)@(spec|test).[jt]s?(x)'
  ]
};
