RxAP changelog service
======================

# Getting started

## Introduction

The RxAP changelog Service is designed to manage and serve changelogs for Angular applications. It enables the
definition of general changelogs applicable across all applications or specific changelogs tailored to
individual applications and their versions.

### Prerequisites

- Docker and Docker Compose installed on your system, with Docker Compose version 3 or higher.

## Installation

Use the following Docker Compose configuration to integrate the RxAP Changelog Service into your setup. The service
comes with Traefik labels included; you only need to define necessary entrypoints for HTTP/HTTPS as per your
requirement.

```yaml
version: "3.7"
services:
   rxap-service-changelog:
      image: registry.gitlab.com/rxap/applications/services/changelog:${RXAP_SERVICE_CHANGELOG:-development}
      volumes:
         - ./shared/service/changelog:/app/assets
```

## Configuration

The service scans the `/app/assets` directory for changelog files. At a minimum, a `latest` folder containing
a `changelog.md` file is required. For version-specific configurations, create folders named after the version (
e.g., `5.4.3`, `6.0.1`) with their respective configuration files.

### Version Determination Logic

The RxAP Changelog Service employs a sophisticated method to determine which version of the changelog to serve
to an application. This logic is especially important for ensuring that applications receive the most compatible
changelog version available.

Here's how the version determination logic works:

- The service maintains a sorted list of all available semantic version numbers for changelogs.
- When an application requests its changelog, the service compares the application's version against this list.
- The service selects the highest version of the changelog that is less than or equal to the application's version.
- If no such version exists (i.e., the application's version is lower than any available changelog version), the
  service defaults to using the configuration from the `latest` folder.

This approach ensures that applications are always provided with the most appropriate changelog for their version,
falling back to a general changelog if no specific match is found.

### Application-Specific Configurations

To define changelogs for a specific application within a version, create a folder named after the application inside
the version folder.

## API Endpoints Overview

The RxAP Changelog Service provides two primary API endpoints for accessing changelogs:

1. **Latest Changelog for an Application:**

   - Endpoint: `/latest/{application}`
   - Method: `GET`
   - Description: Fetches the most recent changelog for the specified application.

2. **Specific Version Changelog for an Application:**

   - Endpoint: `/[version]/{application}`
   - Method: `GET`
   - Description: Fetches the changelog for a specific version of the specified application.

These endpoints ensure that applications can retrieve their necessary changelogs either based on the latest
available or a specific version, depending on their needs.

## Accessing the Service

- The default global API prefix is `api/changelog`, customizable via the `GLOBAL_API_PREFIX` environment variable.
- The service is accessible without authentication.
- Request throttling is enabled; adjust the `THROTTLER_LIMIT` and `THROTTLER_TTL` environment variables as needed.

## API Endpoints

Refer to the provided OpenAPI specification for details on available endpoints, including fetching the latest
changelog for an application or a changelog specific to a version and application.

## Troubleshooting

No known issues are present. Logging level is set to `warn` by default, adjustable via the `LOG_LEVEL` environment
variable (`error`, `warn`, `log`, `debug`, `verbose`).
