import {
  Module,
  Logger
} from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health.controller';

@Module({
  imports: [ TerminusModule ],
  providers: [ ],
  controllers: [ HealthController ]
})
export class HealthModule {
}
