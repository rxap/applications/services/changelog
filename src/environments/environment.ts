import { Environment } from '@rxap/nest-utilities';

export const environment: Environment = {
  name: 'development',
  production: false,
  app: 'changelog',
  sentry: {
    enabled: false,
    debug: false
  }
};
