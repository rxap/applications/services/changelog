import { Environment } from '@rxap/nest-utilities';

export const environment: Environment = {
  name: 'swagger',
  app: 'changelog',
  production: true,
  swagger: true
};
