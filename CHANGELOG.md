# Changelog

## [1.0.2-nightly.3](https://gitlab.com/rxap/applications/services/changelog/compare/v1.0.2-nightly.2...v1.0.2-nightly.3) (2024-07-06)

## [1.0.2-nightly.2](https://gitlab.com/rxap/applications/services/changelog/compare/v1.0.2-nightly.1...v1.0.2-nightly.2) (2024-07-05)

## [1.0.2-nightly.1](https://gitlab.com/rxap/applications/services/changelog/compare/v1.0.2-nightly.0...v1.0.2-nightly.1) (2024-06-30)


### Bug Fixes

* **deps:** update dependency tslib to v2.6.3 ([3d81875](https://gitlab.com/rxap/applications/services/changelog/commit/3d81875385f57c60918764898427c97dd8b9475c))

## [1.0.2-nightly.0](https://gitlab.com/rxap/applications/services/changelog/compare/v1.0.1...v1.0.2-nightly.0) (2024-06-29)


### Bug Fixes

* disable data dir watch by default ([376811e](https://gitlab.com/rxap/applications/services/changelog/commit/376811e402b7dc9fac2f4e7261f00f1ef51cb8eb))

## [1.0.1-nightly.2](https://gitlab.com/rxap/applications/services/changelog/compare/v1.0.1-nightly.1...v1.0.1-nightly.2) (2024-05-27)

## [1.0.1-nightly.1](https://gitlab.com/rxap/applications/services/changelog/compare/v1.0.1-nightly.0...v1.0.1-nightly.1) (2024-05-08)

## [1.0.1-nightly.0](https://gitlab.com/rxap/applications/services/changelog/compare/v1.0.0...v1.0.1-nightly.0) (2024-05-07)
